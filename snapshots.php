<?php

function getUrl($arch)
{
    return "https://download.opensuse.org/repositories/home:/rhabacker:/branches:/windows:/mingw:/win$arch:/snapshots/openSUSE_Leap_15.1/noarch/";
}

$url32 = getUrl("32");
$url64 = getUrl("64");

echo "<!DOCTYPE html>"
    . "<html><body>"
    . "<p>After clicking on the buttons below you will be redirected to the download page to get an Umbrello snapshot</p>"
    . "<ul>"
    . "<li>choose <b>mingw..-umbrello-portable-....rpm</b> to get a zip file with a portable installation of Umbrello</li>"
    . "<li>choose <b>mingw..-umbrello-setup-....rpm</b> to get a setup installer for Umbrello</li>"
    . "<li>Debug symbols can be found in the file <b>mingw..-umbrello-debugpackage-....rpm</b>. They need to be unpacked in the directory above the <b>bin</b> folder
    so that gdb can find them.</li>"
    . "</ul></p>"
    . "<p>The rpm container can be unpacked with 7zip.</p>"
    . "<input type=\"button\" name=\"xxx\" value=\"32-bit snapshot\" onclick=\"javascript:window.location='$url32';\">"
    . "<input type=\"button\" name=\"xxx\" value=\"64-bit snapshot\" onclick=\"javascript:window.location='$url64';\">"
    . "</body></html>"
    ;


