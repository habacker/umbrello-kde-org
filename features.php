<?php
    $page_title = "Umbrello Features";
    include ( "header.inc" );

$ImageFileNameMap = array(
	"AcceptEventAction" => "accept_signal",
	"AcceptTimeEvent" => "accept_time_event",
	"Activity" => "usecase",
	"ActivityNode" => "usecase",
	"ActivityFinal" => "final_activity",
	"AsynchronousMessage" => "umbr-message-asynchronous",
	"Asynchronous Message" => "umbr-coll-message-asynchronous",
	"CentralBuffer" => "object_node",
	"Choice" => "choice-rhomb",
	"CombinedFragment" => "combined_fragment",
	"ControlFlow" => "uniassociation",
	"DeepHistory" => "deep-history",
	"DataStore" => "object_node",
	"DirectionalAssociation" => "uniassociation",
	"Enumeration" => "enum",
	"Implementation" => "realization",
	"ImplementsAssociation" => "generalisation",
	"ExceptionHandler" => "exception",
	"ExpansionRegion" => "region",
	"FinalNode" => "end_state",
	"FinalState" => "end_state",
	"FoundMessage" => "umbr-message-found",
	"FlowFinal" => "final_activity",
	"ForkNode" => "activity-fork",
	"Fork" => "state-fork",
	"InitialState" => "initial_state",
	"InitalNode" => "initial_state",
	"InterruptibleActivityRegion" => "region",
	"InputPin" => "pin",
	"JoinNode" => "activity-fork",
	"Join" => "state-fork",
	"LostMessage" => "umbr-message-lost",
	"MergeNode" => "branch",
	"ObjectFlow" => "uniassociation",
	"ObjectNode" => "object_node",
	"Objects" => "object_node",
	"OutputPin" => "pin",
	"PreCondition" => "PrePostCondition",
	"ReceiveSignal" => "accept_signal",
	"SendSignal" => "send_signal",
	"SendSignalAction" => "send_signal",
	"ShallowHistory" => "shallow-history",
	"State" => "usecase",
	"Synchronous Message" => "umbr-coll-message-synchronous",
	"SynchronousMessage" => "umbr-message-synchronous",
	"PostCondition" => "PrePostCondition",
	"Transformation" => "PrePostCondition",
	"Unidirectional Association" => "uniassociation",
	"Usage" => "dependency",
	"Use case" => "usecase",
	"ValuePin" => "pin",
);

function elementToImageTag($e)
{
	global $ImageFileNameMap;
	if (array_key_exists($e,$ImageFileNameMap))
		$n = $ImageFileNameMap[$e];
	else
		$n = strtolower($e);
	$imageFilePath = "pics/umbrello/$n.png";
	if (file_exists($imageFilePath))
		return "<img src=\"$imageFilePath\">";
	else
		return "";
}

class Table {
    const Unused = -1;
    const No = 0;
    const Yes = 1;
    const Partial = 2;
    const Unknown = 3;
    const InApplicable = 4;
    var $subentry = 0;
    var $showIcons = 0;
       
    function Table($icons=true)
    {
       $this->showIcons = $icons;
    }

    function Header($col1, $col2flag, $col2text, $col3flag, $col3text, $col4flag=Table::Unused, $col4text="")
    {
      ?>
      <style type="text/css">
      #features img {
	  border: 0px;
	  border-radius: 3px 3px 3px 3px;
	  margin: 0px;
	  padding: 0px;
      }      
      th.col1 {
	  width:300px;
      }
      th.col2text {
	  width:100px;
      }
      th.col3text {
	  width:100px;
      }
      th.col4text {
	  width:120px;
      }
      td {
	  vertical-align: middle;
	  }
      </style>
      <?php

      echo "<div id=features><table width=\"100%\">\n";
      $this->headerLine = "<tr><th colspan=2 class=\"col1\"></th>"
	."<th></th><th class=\"col2text\"></th>"
	."<th colspan=2></th>"
	. ($col4flag != Table::Unused ? "<th></th><th class=\"col4text\"></th>" : "")
	."</tr>\n"
	."<tr><th colspan=2 class=\"col1\">$col1</th>"
	."<th>$col2flag</th><th class=\"col2text\">$col2text</th>"
	."<th>$col3flag</th><th class=\"col3text\">$col3text</th>"
	. ($col4flag != Table::Unused ? "<th>$col4flag</th><th class=\"col4text\">$col4text</th>" : "")
	."</tr>\n";
      $this->lineHeader();
    }

    static function withDetails()
    {
          return isset($_REQUEST['details']);
    }

    function lineHeader()
    {
      echo $this->headerLine;
    }

    function colEntry($value)
    {
      switch($value) {
      case self::Yes:
	  return "<img class=\"features\" src=\"pics/dialog-ok.png\">";
      case self::Partial:
	  return "(<img class=\"features\" src=\"pics/dialog-partly.png\">)";
	  //return "<img class=\"features\" src=\"pics/dialog-partly.png\">";
      case self::Unknown:
	  return "???";
      case self::InApplicable:
	  return "---";
      case self::No:
      default: 
	  return "";
      }
    }

    function Entry($col1text, $col2flag, $col2text, $col3flag, $col3text, $col4flag=Table::Unused, $col4text="")
    {
      $td = "<td align=\"center\">";
      if ($this->withDetails()) {
	  if ($this->subentry) {
	      $this->SubEntryEnd();
	      $this->lineHeader();
	  }
      }
		echo "<tr>";
		$tag = "";
		if (strpos($col1text, "Diagrams") !== false) {
			$n = strtolower(str_replace(" ","", str_replace("Diagrams","",$col1text)));
			$imageFilePath = "pics/umbrello/hi16-action-umbrello_diagram_$n.png";
			if (file_exists($imageFilePath))
				$tag = "<img src=\"$imageFilePath\">";
		} else
			$tag = elementToImageTag($col1text);
		if ($this->showIcons && $tag != "")
			echo "<td>$tag</td><td>$col1text</td>";
		else
			echo "<td></td><td>$col1text</td>";
	echo
	  $td.$this->colEntry($col2flag)."</td>".$td.htmlentities($col2text)."</td>"
	. $td.$this->colEntry($col3flag)."</td>".$td.htmlentities($col3text)."</td>"
	. ($col4flag != Table::Unused ? $td.$this->colEntry($col4flag)."</td>".$td.htmlentities($col4text)."</td>" : "")
	. "</tr>\n";
      $this->subentry = 0;
    }

    function SubEntry($col1text, $col2flag, $col2text, $col3flag, $col3text, $col4flag=Table::Unused, $col4text="")
    {
      $td = "<td align=\"center\">";
      if ($this->withDetails()) {
        if ($this->subentry == 0)
			echo "<tr><th colspan=3>Diagram Elements</th><th colspan=5></th></tr>";
		echo "<tr><td>".($this->showIcons ? elementToImageTag($col1text) : '')."</td><td>$col1text</td>"
		. $td.$this->colEntry($col2flag)."</td>".$td.htmlentities($col2text)."</td>"
		. $td.$this->colEntry($col3flag)."</td>".$td.htmlentities($col3text)."</td>"
		. ($col4flag != Table::Unused ? $td.$this->colEntry($col4flag)."</td>".$td.htmlentities($col4text)."</td>" : "")
		. "</tr>\n";
		$this->subentry++;
      }
    }

    function SubEntryEnd()
    {
	  echo "<tr><td colspan=7></td></tr>\n";
    }

    function Footer()
    {
      echo "</table></div>\n";
    }

    function legend()
    {
      echo "<p>Legend</br><div id=features><table>"
      ."<tr><td>".$this->colEntry(Table::No)."</td><td>Not implemented</td></tr>"
      ."<tr><td>".$this->colEntry(Table::Partial)."</td><td>Partially implemented</td></tr>"
      ."<tr><td>".$this->colEntry(Table::Yes)."</td><td>Available/implemented</td></tr>"
      ."<tr><td>".$this->colEntry(Table::InApplicable)."</td><td>Inapplicable</td></tr>"
      ."<tr><td>".$this->colEntry(Table::Unknown)."</td><td>Unknown (not evaluated yet)</td></tr>"
      ."</table></div></p>";
    }
};
?>

<p>
Most umbrello features and diagram types depends on the public <a href="http://en.wikipedia.org/wiki/Unified_Modeling_Language">UML standards</a>. 
</p>
<img width=600px src="http://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/UML_diagrams_overview.svg/792px-UML_diagrams_overview.svg.png">
<?php
if (!Table::withDetails())
	echo '<p>To see what kind of diagram elements are supported, click <a href="features.php?details=1">here</a>.</p>';
else
	echo '<p>To collapse details, click <a href="features.php">here</a>.</p>';
?>
<h2>Structure Diagrams</h2>
<?php
$uml1_4 = "<a href=\"http://www.omg.org/spec/UML/1.4/\">UML 1.4</a>";
$uml2_0 = "<a href=\"http://www.omg.org/spec/UML/2.0/\">UML 2.0</a>";
$t = new Table;
$t->Header("Diagram Type", $uml1_4, "Notes", $uml2_0, "Notes", "", "");
$t->Entry("Class Diagrams", Table::Yes, "", Table::Partial, "Interface: no --o or --( notation Usage:Dependency with <<use>> text");
$t->SubEntry("Association", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Aggregation", Table::Yes, "", Table::Yes, "");
$t->SubEntry("DirectionalAssociation", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Class", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Composition", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Containment", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Dependency", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Enumeration", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Generalisation", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Interface", Table::Yes, "", Table::Partial, "no --o or --( notation");
//$t->SubEntry("InterfaceRealisation", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Package", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Realization", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Usage", Table::InApplicable, "", Table::Partial, "Dependency with <<use>> text");

$t->Entry("Component Diagrams", Table::Yes, "", Table::Partial, "only UML 1.x symbol");
$t->SubEntry("Artifact", Table::No, "", Table::Unknown, "Variants: Default File Library Table");
$t->SubEntry("Association", Table::Yes, "", Table::Unknown, "");
$t->SubEntry("Component", Table::Yes, "", Table::Partial, "only UML 1.x symbol");
$t->SubEntry("Dependency", Table::Yes, "", Table::Unknown, "");
$t->SubEntry("Implementation", Table::Yes, "", Table::Unknown, "");
$t->SubEntry("Interface", Table::Yes, "", Table::Unknown, "");
$t->SubEntry("Component implements Interface", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Component has provided Port", Table::InApplicable, "", Table::No, "Should be on component edges");
$t->SubEntry("Component uses Interface", Table::InApplicable, "", Table::No, "To be implement as half circle");
$t->SubEntry("Component has complex Port", Table::InApplicable, "", Table::No, "");

$t->Entry("Object Diagrams", Table::InApplicable, "", Table::Partial, "Using deployment diagrams");
$t->SubEntry("InstanceSpecification", Table::InApplicable, "", Table::Partial, "use object widget");
$t->SubEntry("Link (i.e. Association)", Table::InApplicable, "", Table::Partial, "use Assocication");

$t->Entry("Profile Diagrams", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Stereotypes", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Profiles", Table::InApplicable, "", Table::No, "");

$t->Entry("Composite Structure Diagrams", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Part", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Port", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Collaboration", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Collaboration Use", Table::InApplicable, "", Table::No, "");

$t->Entry("Deployment Diagrams", Table::Yes, "", Table::Partial, "");
$t->SubEntry("Artifact", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Artifact deployed on Node", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Artifact with annotated deployment properties", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Deployment specification", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Deployment specification - with properties", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Deployment specification - with property values", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Interface", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Node", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Node with deployed Artifacts", Table::InApplicable, "", Table::Partial, "only textual notation");
$t->SubEntry("Object", Table::Yes, "", Table::Unknown, "");
$t->SubEntry("Association", Table::Yes, "", Table::Partial, "Only undirected");
$t->SubEntry("Dependency", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Generalisation", Table::Yes, "", Table::Partial, "Named 'Implements'");
$t->SubEntry("Deployment", Table::InApplicable, "", Table::Partial, "Dependency with <<deploy>> text");
$t->SubEntry("Manifestation", Table::InApplicable, "", Table::Partial, "Dependency with <<manifest>> text");

$t->Entry("Package Diagrams", Table::InApplicable, "", Table::Partial, "Using class diagrams");
$t->SubEntry("Dependency", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Package", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Package Merge", Table::InApplicable, "", Table::Partial, "Dependency with <<merge>> name");
$t->SubEntry("PackageImport (public)", Table::InApplicable, "", Table::Partial, "Dependency with <<import>> name");
$t->SubEntry("PackageImport (privat)", Table::InApplicable, "", Table::Partial, "Dependency with <<access>> name");
$t->Footer();

echo "<h2>Behavior Diagrams</h2>\n";

$t = new Table;
$t->Header("Diagram Type", $uml1_4, "Notes", $uml2_0, "Notes", "", "");
$t->Entry("Activity Diagrams", Table::Yes, "", Table::Partial, "");
$t->SubEntry("AcceptEventAction", Table::Yes, "", Table::Yes, "");
$t->SubEntry("AcceptTimeEvent", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("Action", Table::Unknown, "", Table::No, "");
$t->SubEntry("Activity", Table::Yes, "", Table::No, "");
$t->SubEntry("ActivityPartition", Table::Unknown, "", Table::No, "");
$t->SubEntry("ActivityEdge", Table::Unknown, "", Table::No, "");
$t->SubEntry("ActivityFinal", Table::Yes, "", Table::Yes, "");
$t->SubEntry("ActivityNode", Table::No, "", Table::Yes, "");
$t->SubEntry("CentralBuffer", Table::Yes, "", Table::Yes, "");
$t->SubEntry("ControlFlow", Table::Unknown, "", Table::Yes, "");
// Base type for Nodes
//$t->SubEntry("ControlNode", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("DataStore", Table::Yes, "", Table::Yes, "");
$t->SubEntry("ExceptionHandler", Table::Yes, "", Table::Partial, "named 'Exception'");
$t->SubEntry("ExpansionRegion", Table::Yes, "", Table::Yes, "");
$t->SubEntry("FinalNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("FlowFinal", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("ForkNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("InitalNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("InputPin", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("InterruptibleActivityRegion", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("JoinNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("MergeNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("ObjectFlow", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("ObjectNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("OutputPin", Table::Unknown, "", Table::Yes, "");
$t->SubEntry("PreCondition", Table::Yes, "", Table::Yes, "");
$t->SubEntry("PostCondition", Table::Yes, "", Table::Yes, "");
$t->SubEntry("SendSignalAction", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Transformation", Table::Yes, "", Table::Yes, "");
$t->SubEntry("ValuePin", Table::Unknown, "", Table::Yes, "");

$t->Entry("Use Case Diagrams", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Actor", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("Association", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("DirectionalAssociation", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("Dependency", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("ImplementsAssociation", Table::Unknown, "", Table::Unknown, "");
$t->SubEntry("Extend", Table::InApplicable, "", Table::Partial, "Dependency with <<extend>> text");
$t->SubEntry("Extend (with condition)", Table::InApplicable, "", Table::No, "");
$t->SubEntry("ExtensionPoint", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Include", Table::InApplicable, "", Table::Partial, "Dependency with <<include>> text");
$t->SubEntry("UseCase", Table::Yes, "", Table::Partial, "ellipse only");

$t->Entry("State Diagrams", Table::Yes, "", Table::Yes, "");
$t->SubEntry("Action", Table::InApplicable, "", Table::No, "");
$t->SubEntry("CallEvent", Table::No, "Event", Table::InApplicable, "");
$t->SubEntry("ChangeEvent", Table::No, "Event", Table::InApplicable, "");
$t->SubEntry("Choice", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("CompositeState", Table::No, "State", Table::No, "State");
$t->SubEntry("Constraint", Table::InApplicable, "", Table::InApplicable, "UML 2.2(VP)");
$t->SubEntry("DeepHistory", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("Event", Table::No, "", Table::InApplicable, "");
$t->SubEntry("EntryPoint", Table::InApplicable, "", Table::No, "PseudoState");
$t->SubEntry("ExitPoint", Table::InApplicable, "", Table::No, "PseudoState");
$t->SubEntry("FinalState", Table::Yes, "named End State", Table::Yes, "");
$t->SubEntry("Fork", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("Guard", Table::No, "", Table::No, "");
$t->SubEntry("InitialState", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("Interface", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Join", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("Junction", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("Port", Table::InApplicable, "", Table::No, "");
$t->SubEntry("Region", Table::InApplicable, "", Table::No, "");
$t->SubEntry("ReceiveSignal", Table::InApplicable, "", Table::No, "");
$t->SubEntry("SendSignal", Table::InApplicable, "", Table::No, "");
$t->SubEntry("ShallowHistory", Table::InApplicable, "", Table::Yes, "PseudoState");
$t->SubEntry("SignalEvent", Table::No, "Event", Table::InApplicable, "");
$t->SubEntry("SimpleState", Table::No, "State", Table::No, "State");
$t->SubEntry("State", Table::Yes, "with activities", Table::Yes, "");
$t->SubEntry("StubState", Table::No, "State", Table::InApplicable, "");
$t->SubEntry("SubmachineState", Table::InApplicable, "", Table::No, "State");
$t->SubEntry("SyncState", Table::No, "State", Table::InApplicable, "");
$t->SubEntry("Terminate", Table::InApplicable, "", Table::No, "PseudoState");
$t->SubEntry("TimeEvent", Table::No, "Event", Table::No, "");
$t->SubEntry("Transition", Table::Yes, "named State Transition", Table::Yes, "");

$t->Entry("Sequence Diagrams", Table::Yes, "", Table::Partial, "no multi objects");
$t->SubEntry("ObjectNode", Table::Yes, "", Table::Yes, "");
$t->SubEntry("AsynchronousMessage", Table::Yes, "", Table::Yes, "");
$t->SubEntry("CombinedFragment", Table::InApplicable, "", Table::Yes, "Reference Option Break Loop Negative Critical Assertion Alternative Parallel");
$t->SubEntry("FoundMessage", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("LostMessage", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Precondition", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("SynchronousMessage", Table::Yes, "", Table::Yes, "");

$t->Entry("Collaboration Diagrams", Table::Yes, "", Table::InApplicable, "");
$t->SubEntry("ObjectNode", Table::Yes, "", Table::InApplicable, "");
$t->SubEntry("Asynchronous Message", Table::Yes, "", Table::InApplicable, "");
$t->SubEntry("Synchronous Message", Table::Yes, "", Table::InApplicable, "");

$t->Entry("Communication Diagrams", Table::InApplicable, "", Table::Partial, "Using Collaboration Diagrams");
$t->SubEntry("ObjectNode", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Asynchronous Message", Table::InApplicable, "", Table::Yes, "");
$t->SubEntry("Synchronous Message", Table::InApplicable, "", Table::Yes, "");

$t->Entry("Interaction Overview Diagrams", Table::InApplicable, "", Table::No, "");
$t->Entry("Timing Diagrams", Table::InApplicable, "", Table::No, "");
$t->Entry("Entity Relationship Diagrams", Table::InApplicable, "No UML standard", Table::InApplicable, "No UML standard");
$t->Entry("Extended Entity Relationship (EER) Diagram Concepts", Table::InApplicable, "No UML standard", Table::InApplicable, "No UML standard");
$t->Footer();
?>
</p>

<p>
The following UML Icons are supported:
<?php
$t = new Table(true);
$t->Header("Icon Type", $uml1_4, "Notes", $uml2_0, "Notes", "", "");
$t->Entry("Actor", Table::Yes, "", Table::Yes, "");
$t->Entry("Use case", Table::Yes, "", Table::Yes, "");
$t->Entry("Classes/Concepts", Table::Yes, "", Table::Yes, "");
$t->Entry("Objects", Table::Yes, "", Table::Yes, "");
$t->Entry("Line of Texts", Table::Yes, "", Table::Yes, "");
$t->Entry("Table::Note Boxes", Table::Yes, "", Table::Yes, "");
$t->Footer();
?>

<p>
The following associations are supported:
<?php
$t = new Table(true);
$t->Header("Association Type", $uml1_4, "Notes", $uml2_0, "Notes", "", "");
$t->Entry("Anchor", Table::Yes, "", Table::Yes, "");
$t->Entry("Aggregation", Table::Yes, "", Table::Yes, "");
$t->Entry("Association", Table::Yes, "", Table::Yes, "");
$t->Entry("Composition", Table::Yes, "", Table::Yes, "");
$t->Entry("Containment", Table::Unknown, "", Table::Unknown, "");
$t->Entry("Dependency", Table::Yes, "", Table::Yes, "");
$t->Entry("Exception", Table::Yes, "", Table::Yes, "");
$t->Entry("Generalisation", Table::Yes, "", Table::Yes, "");
$t->Entry("Implementation", Table::Yes, "", Table::Yes, "");
$t->Entry("Realization", Table::Yes, "", Table::Yes, "");
$t->Entry("Relationship", Table::InApplicable, "", Table::InApplicable, "");
$t->Entry("Unidirectional Association", Table::Yes, "", Table::Yes, "");
$t->Footer();
?>
</p>

<p>
The following graphics primitives are supported on all diagram types:
<?php
$t = new Table();
$t->Header("graphics primitives", $uml1_4, "Notes", $uml2_0, "Notes", "", "");
$t->Entry("Anchor", Table::Yes, "", Table::Yes, "");
$t->Entry("Box", Table::Yes, "", Table::Yes, "");
$t->Entry("Label", Table::Yes, "", Table::Yes, "");
$t->Entry("Note", Table::Yes, "", Table::Yes, "");
$t->Footer();
?>
</p>


<p>Supported languages of code generators and code importers: 
<?php
$t = new Table(false);
$t->Header("Language","Code Generation", "Notes", "Code Import", "Notes", "", "");
$t->Entry("ActionScript", Table::Yes, "", Table::No, "");
$t->Entry("Ada", Table::Yes, "", Table::Yes, "");
$t->Entry("C++", Table::Yes, "", Table::Yes, "");
$t->Entry("C#", Table::Yes, "", Table::Yes, "");
$t->Entry("D", Table::Yes, "", Table::No, "");
$t->Entry("IDL", Table::Yes, "", Table::Yes, "");
$t->Entry("Java™", Table::Yes, "", Table::Yes, "");
$t->Entry("JavaScript", Table::Yes, "", Table::No, "");
$t->Entry("MySQL", Table::Yes, "", Table::Yes, "");
$t->Entry("Pascal", Table::Yes, "", Table::Yes, "");
$t->Entry("Perl", Table::Yes, "", Table::No, "");
$t->Entry("PHP", Table::Yes, "", Table::Yes, ">= 2.24.0");
$t->Entry("PHP5", Table::Yes, "", Table::Yes, ">= 2.24.0");
$t->Entry("PostgreSQL", Table::Yes, "", Table::Yes, "");
$t->Entry("Python", Table::Yes, "", Table::Yes, "");
$t->Entry("Ruby", Table::Yes, "", Table::No, "");
$t->Entry("SQL", Table::Yes, "", Table::No, "");
$t->Entry("Tcl", Table::Yes, "", Table::No, "");
$t->Entry("Vala", Table::Yes, "", Table::No, "");
$t->Entry("XMLSchema", Table::Yes, "", Table::No, "");
$t->Footer();
$t->legend();
?>
<p>

<h2>Supported XMI file formats</h2>
<p>
Umbrello supports XMI 1.2 file import and export (nearly compatible to UML 1.4).
Support for XMI 2.0 is currently in work. See <a href="https://bugs.kde.org/show_bug.cgi?id=115269">Feature Request 115269</a> for more informations.
</p>

<h2>Supported 3rdparty file import formats</h2>
<p>
<ul>
<li>Argo UML</li>
<li>Enterprise Architect</li>
<li>NSUML</li>
<li>Poseidon for UML</li>
<li>UNISYS</li>
</p>

<?php
    include ( "footer.inc" );
?>
