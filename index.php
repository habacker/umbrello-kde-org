<?php
    //Header("Location: http://uml.sourceforge.net");
    $page_title = "Welcome to Umbrello - The UML Modeller";
    include ( "header.inc" );
?>

<div style="float: right;">
<a href="screenshots/umbrello-2016.png"><img src="screenshots/umbrello-2016-wee.png" width="400" height="249" /></a>
</div>

<p>Umbrello UML Modeller is a Unified Modelling Language (UML) diagram program based on KDE Technology.</p>
<p>UML allows you to create diagrams of software and other systems in a standard format to document or design the structure of your programs.</p>
<p>You may take a look at the <a href="screenshots.php">screenshots</a> to see umbrello in action.</p>
<p>Our <a href="documentation.php">handbook</a> gives a good introduction to Umbrello and UML modelling.</p>
<p>Umbrello comes with KDE SC, included with every Linux distribution and available through your package manager and as installer for Windows and Mac OS X. See <a href="installation.php">Installation</a> to install Umbrello.</p>
<p>For user support join the <a href="https://mail.kde.org/mailman/listinfo/umbrello">Umbrello mailing list</a>.</p>
<p>We are an open source project who welcomes all developers and contributors.  Join the <a href="https://mail.kde.org/mailman/listinfo/umbrello-devel">Umbrello development mailing list</a> and say hi</a>.</p>

<div style="float: left;margin-top:-5px">
<iframe src="https://www.openhub.net/p/308/widgets/project_thin_badge.html" scrolling="no" marginHeight=0 marginWidth=0 style="height: 30px; width: 145px; border: none;"></iframe>
</div>
<div style="float: right;margin-top:-110px;margin-right:20px;">
Download <a href="http://download.kde.org/stable/umbrello/latest/">Umbrello latest release for Windows</a></br/>
Download <a href="https://www.macports.org/ports.php?by=name&substr=umbrello">Umbrello for Mac OS X on MacPorts</a><br/>
Download <a href="https://snapcraft.io/umbrello">Umbrello for Linux as Snap package</a><br/>
</div>
<!--
<div style="float: right;margin-top:-20px;">
<a href="https://scan.coverity.com/projects/3327">
  <img alt="Coverity Scan Build Status"
       src="https://scan.coverity.com/projects/3327/badge.svg"/>
</a>
</div>
-->
<?php
    kde_general_news("./news.rdf", 10, true);
    include ( "footer.inc" );
?>
